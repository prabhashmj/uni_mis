<div class="span3" id="sidebar">
	<img id="avatar" class="img-polaroids" src="admin/<?php echo $row['location']; ?>">
	<?php include('lecturer_count.php'); ?>
	<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
		<li class=""><a href="dasboard_lecturer.php"><i class="icnchevron-right"></i><i class="icngroup"></i>&nbsp;My Courses</a></li>
		<li class=""><a href="notification_lecturer.php"><i class="icnchevron-right"></i><i class="icninfo-sign"></i>&nbsp;Notification
			<?php if($not_read == '0'){
				}else{ ?>
					<span class="badge badge-important"><?php echo $not_read; ?></span>
				<?php } ?>
		</a></li>
		<li class=""><a href="lecturer_message.php"><i class="icnchevron-right"></i><i class="icnenvelope-alt"></i>&nbsp;Message</a></li>
		<li class=""><a href="lecturer_backack.php"><i class="icnchevron-right"></i><i class="icnsuitcase"></i>&nbsp;Faculty Notices</a></li>
		<li class=""><a href="add_downloadable.php"><i class="icnchevron-right"></i><i class="icnplus-sign"></i>&nbsp;Add Materials</a></li>
		<li class=""><a href="add_announcement.php"><i class="icnchevron-right"></i><i class="icnplus-sign"></i>&nbsp;Add Announcement</a></li> 
		<li class="active"><a href="add_assignment.php"><i class="icnchevron-right"></i><i class="icnplus-sign"></i>&nbsp;Add Assignment</a></li>
		<li class=""><a href="lecturer_quiz.php"><i class="icnchevron-right"></i><i class="icnlist"></i>&nbsp;Quiz</a></li>
<!--		<li class=""><a href="lecturer_share.php"><i class="icnchevron-right"></i><i class="icnfile"></i>&nbsp;Shared Files</a></li>-->
	</ul>
<!--	--><?php //include('search_other_class.php'); ?><!--	-->
</div>

