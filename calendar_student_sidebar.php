<div class="span3" id="sidebar">
	<img id="avatar" src="admin/<?php echo $row['location']; ?>" class="img-polaroids">
		<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
				<li class=""><a href="dashboard_student.php"><i class="icnchevron-right"></i><i class="icnchevron-left"></i>&nbsp;Back</a></li>
				<li class=""><a href="my_classmates.php<?php echo '?id='.$get_id; ?>"><i class="icnchevron-right"></i><i class="icngroup"></i>&nbsp;My Collegues</a></li>
				<li class=""><a href="progress.php<?php echo '?id='.$get_id; ?>"><i class="icnchevron-right"></i><i class="icnbar-chart"></i>&nbsp;My Progress</a></li>
<!--				<li class=""><a href="subject_overview_student.php--><?php //echo '?id='.$get_id; ?><!--"><i class="icnchevron-right"></i><i class="icnfile"></i>&nbsp;Subject Overview</a></li>-->
				<li class=""><a href="downloadable_student.php<?php echo '?id='.$get_id; ?>"><i class="icnchevron-right"></i><i class="icndownload"></i>&nbsp;Downloadable Materials</a></li>
				<li class=""><a href="assignment_student.php<?php echo '?id='.$get_id; ?>"><i class="icnchevron-right"></i><i class="icnbook"></i>&nbsp;Assignments</a></li>
				<li class=""><a href="announcements_student.php<?php echo '?id='.$get_id; ?>"><i class="icnchevron-right"></i><i class="icninfo-sign"></i>&nbsp;Announcements</a></li>
				<li class="active"><a href="class_calendar_student.php<?php echo '?id='.$get_id; ?>"><i class="icnchevron-right"></i><i class="icncalendar"></i>&nbsp;Course Calendar</a></li>
				<li class=""><a href="student_quiz_list.php<?php echo '?id='.$get_id; ?>"><i class="icnchevron-right"></i><i class="icnreorder"></i>&nbsp;Quiz</a></li>
		</ul>
<!--	--><?php ///* include('search_other_class.php');  */?><!--	-->
</div>