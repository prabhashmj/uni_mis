<?php include('header.php'); ?>
<html>

<style>
    body {font-family: Arial, Helvetica, sans-serif;
        background: url(images/index0.jpg) no-repeat center center fixed;
        -webkit-background-size: cover !important;
        -moz-background-size: cover !important;
        -o-background-size: cover !important;
        background-size: cover !important; }
    form {border: 3px solid #f1f1f1;}

    input[type=text], input[type=password] {
        width: 90%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    button {
        background-color: #04AA6D;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 80%;
    }

    button:hover {
        opacity: 0.8;
    }




    .container {
        padding: 16px;
    }

    /*span.psw {*/
        /*float: right;*/
        /*padding-top: 16px;*/
    /*}*/

    /*!* Change styles for span and cancel button  on extra small screens *!*/
    /*@media screen and (max-width: 300px) {*/
        /*span.psw {*/
            /*display: block;*/
            /*float: none;*/
        /*}*/

    }
</style>
</head>
<body id="login" style="text-align: center;">

<h2>Admin Login</h2>

<form id="login_form" class="form-signin" style="display: inline-block; width: 700px;"  method="post">


<!--    <div class="container">-->
        <label  for="username"><b>Username</b></label>
        <input class="input-block-level" type="text" placeholder="Enter Username" id="usernmae" name="username" required>

        <label for="password"><b>Password</b></label>
        <input class="input-block-level" type="password" placeholder="Enter Password" id="password" name="password" required>
<hr>
        <button name="login" type="submit">Login</button>
        <label>
            <input type="checkbox" checked="checked" name="remember"> Remember me
        </label>
<!--    </div>-->

<!--    <div class="container" style="background-color:#f112cf">-->
        <span class="psw">Forgot <a href="forgot_password.php">password?</a></span>
<!--    </div>-->
</form>

<script>
    jQuery(document).ready(function(){
        jQuery("#login_form").submit(function(e){
            e.preventDefault();
            var formData = jQuery(this).serialize();
            $.ajax({
                type: "POST",
                url: "login.php",
                data: formData,
                success: function(html){
                    if(html=='true')
                    {
                        $.jGrowl("Welcome University Management Information System", { header: 'Access Granted' });
                        var delay = 2000;
                        setTimeout(function(){ window.location = 'dashboard.php'  }, delay);
                    }
                    else if(html=='true_ma')
                    {
                        $.jGrowl("Welcome University Management Information System", { header: 'Access Granted' });
                        var delay = 2000;
                        setTimeout(function(){ window.location = 'dashboard_ma.php'  }, delay);
                    }
                    else
                    {
                        $.jGrowl("Please Check your username and Password", { header: 'Login Failed' });
                    }
                }

            });
            return false;
        });
    });
</script>

</body>
</html><!-- /container
<?php include('script.php'); ?>
<!--<body id="login" style=" text-align: center;">-->
<!--    <div class="container">-->
<!---->
<!--      <form id="login_form" style="display: inline-block;" class="form-signin" method="post">-->
<!--        <h3 class="form-signin-heading"><i class="icnlock"></i> Please Login</h3>-->
<!--        <input type="text" class="input-block-level" id="usernmae" name="username" placeholder="Username" required>-->
<!--        <input type="password" class="input-block-level" id="password" name="password" placeholder="Password" required>-->
<!--        <button name="login" class="btn btn-info" type="submit"><i class="icnsignin icn"></i> Sign in</button>-->
<!---->
<!--		      </form>-->
<!--				<script>-->
<!--			jQuery(document).ready(function(){-->
<!--			jQuery("#login_form").submit(function(e){-->
<!--					e.preventDefault();-->
<!--					var formData = jQuery(this).serialize();-->
<!--					$.ajax({-->
<!--						type: "POST",-->
<!--						url: "login.php",-->
<!--						data: formData,-->
<!--						success: function(html){-->
<!--						if(html=='true')-->
<!--						{-->
<!--						$.jGrowl("Welcome University Management Information System", { header: 'Access Granted' });-->
<!--						var delay = 2000;-->
<!--							setTimeout(function(){ window.location = 'dashboard.php'  }, delay);-->
<!--						}-->
<!--						else if(html=='true_ma')-->
<!--                            {-->
<!--                                $.jGrowl("Welcome University Management Information System", { header: 'Access Granted' });-->
<!--                                var delay = 2000;-->
<!--                                setTimeout(function(){ window.location = 'dashboard_ma.php'  }, delay);-->
<!--                            }-->
<!--						else-->
<!--						{-->
<!--						$.jGrowl("Please Check your username and Password", { header: 'Login Failed' });-->
<!--						}-->
<!--						}-->
<!---->
<!--					});-->
<!--					return false;-->
<!--				});-->
<!--			});-->
<!--			</script>-->
<!---->
<!---->
<!---->
<!---->
<!--    </div>-->
<!--  </body>-->