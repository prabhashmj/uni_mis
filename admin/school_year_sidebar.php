     <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li> <a href="dashboard.php"><i class="icnchevron-right"></i><i class="icnhome"></i>&nbsp;Dashboard</a> </li>
						<li>
                            <a href="subjects.php"><i class="icnchevron-right"></i><i class="icnlist-alt"></i> Subject</a>
                        </li>
						<li>
                            <a href="class.php"><i class="icnchevron-right"></i><i class="icngroup"></i> Class</a>
                        </li>
						<li  class="">
                            <a href="admin_user.php"><i class="icnchevron-right"></i><i class="icnuser"></i> Admin Users</a>
                        </li>
						<li>
                            <a href="department.php"><i class="icnchevron-right"></i><i class="icnbuilding"></i> Department</a>
                        </li>
						<li>
                            <a href="students.php"><i class="icnchevron-right"></i><i class="icngroup"></i> Students</a>
                        </li>
						<li>
                            <a href="lecturers.php"><i class="icnchevron-right"></i><i class="icngroup"></i> Lecturers</a>
                        </li>
							<li>
                            <a href="downloadable.php"><i class="icnchevron-right"></i><i class="icndownload"></i> Downloadable Materials</a>
                        </li>
						<li>
                            <a href="assignment.php"><i class="icnchevron-right"></i><i class="icnupload"></i> Uploaded Assignments</a>
                        </li>
						<li>
                            <a href="content.php"><i class="icnchevron-right"></i><i class="icnfile"></i> Content</a>
                        </li>
						<li>
                            <a href="user_log.php"><i class="icnchevron-right"></i><i class="icnfile"></i> User Log</a>
                        </li>
						<li>
                            <a href="activity_log.php"><i class="icnchevron-right"></i><i class="icnfile"></i> Activity Log</a>
                        </li>
						<li class="active">
                            <a href="school_year.php"><i class="icnchevron-right"></i><i class="icncalendar"></i> Academic Year</a>
                        </li>
						<li class="">
                            <a href="calendar_of_events.php"><i class="icnchevron-right"></i><i class="icncalendar"></i>Calendar of Events</a>
                        </li>
                    </ul>
					
					
				
                </div>