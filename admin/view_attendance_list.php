<?php  include('header.php'); ?>
<?php  include('session.php'); ?>
    <body>
		<?php include('navbar.php') ?>
        <div class="container-fluid">
            <div class="row-fluid">
					<?php include('sidebar_dashboard_ma.php'); ?>
                <!--/span-->
                <div class="span9" id="content">
						<div class="row-fluid"></div>
						
                    <div class="row-fluid">
                        <a onclick="window.location='view_attendance.php'" id="btn_login" name="list" class="btn btn-info" type="submit"><i class="icnsignin icnlarge"></i>Graphical View</a>

                        <!-- block -->
                        <div id="block_bg" class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">List View of Attendance Percentages</div>
                            </div>
                            <div class="block-content collapse in">
							        <div class="span12">

                                        <?php
                                        $query = "SELECT * FROM student";


                                        echo '<table border="0" cellspacing="2" cellpadding="2">
                                            <tr>
                                                <td> <font face="Arial">Student ID</font> </td>
                                                <td> <font face="Arial">First Name</font> </td>
                                                <td> <font face="Arial">Last Name</font> </td>
                                                <td> <font face="Arial">Attendance Percentage</font> </td>
                                            </tr>';

                                            if ($result = $conn->query($query)) {
                                            while ($row = $result->fetch_assoc()) {
                                            $field1name = $row["student_id"];
                                            $field2name = $row["firstname"];
                                            $field3name = $row["lastname"];
                                            $field4name = $row["marked_attendance"]."%";

                                            echo '<tr>
                                                <td>'.$field1name.'</td>
                                                <td>'.$field2name.'</td>
                                                <td>'.$field3name.'</td>
                                                <td>'.$field4name.'</td>
                                            </tr>';
                                            }
                                            $result->free();
                                            }
                                            ?>
						
                            </div>
                        </div>
                        <!-- /block -->
						
                    </div>
                    </div>
                
                
                 
                 
                </div>
            </div>
    
         <?php include('footer.php'); ?>
        </div>
	<?php include('script.php'); ?>
    </body>

</html>