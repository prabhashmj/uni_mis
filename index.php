<?php include('header.php'); ?>
<style>
	body#login::before {
    content: "";
    background: #00000036;
    position: absolute;
    top: 0;
    /* z-index: 1; */
    left: 0;
    width: 100%;
    height: 100%;
}
	
</style>
<body id="login">

<div class="container" style="position: relative"><a href="index.php"><img  class="index_logo pull-right" src="admin/images/Logo.png"></a>
		<div class="row-fluid">
			<div class="span6"><div class="pull-left"><?php include('login_form.php'); ?></div></div>
            <div class="span6"><div class="title_index"><?php include('title_index.php'); ?></div></div>

        </div>
		<div class="row-fluid">
			<div class="span12" style="margin-top: 20px;"><div class="index-footer"><?php include('link.php'); ?></div></div>
		</div>
			<?php include('footer.php'); ?>
    </div>
<?php include('script.php'); ?>
</body>
</html>